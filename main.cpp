#include "EyeGazing.h"

int main( int argc, const char** argv )
{
    CommandLineParser parser(argc, argv,
                         "{help h||}"
                         "{face_cascade|../../data/haarcascades/haarcascade_frontalface_alt.xml|Path to face cascade.}"
                         "{eyes_cascade|../../data/haarcascades/haarcascade_eye_tree_eyeglasses.xml|Path to eyes cascade.}"
                         "{camera|0|Camera device number.}");

    
    //get the camera
    VideoCapture capture;
    int camera_device = parser.get<int>("camera");
    //Read the video stream
    capture.open(camera_device);
    if (!capture.isOpened())
    {
        cout << "--(!)Error opening video capture\n";
        return -1;
    }

    int currentFps = 30;
    int ixFps = 0;
    int rate = 30;
    int newFps = currentFps/rate;

    Mat frame;
    EyeGazing e;
    int ixFrame = 0;
    e.openFiles();
    while (capture.read(frame))
    {
        if( frame.empty() )
        {
            cout << "--(!) No captured frame -- Break!\n";
            break;
        }

        //Apply the classifier to the frame
        if(ixFps%newFps==0){
            //mirror efect
            cv::flip(frame, frame, 1);
            //main function
            e.detectAndDisplay(ixFrame, frame);
            ixFps = 0;
        }
        
        if( waitKey(10) == 27 )
        {
            e.closeFiles();
            //e->~EyeGazing();
            break; // escape
        }

        ixFps++;
        ixFrame++;
    }
    return 0;
}