#include "EyeGazing.h"

EyeGazing::EyeGazing(){

}

void EyeGazing::openFiles()
{
    //(x,y) log
    LogFile.open ("Log.txt");
    //Load the face cascades
    String face_cascade_name = "xml/haarcascade_frontalface_alt.xml";
    if(!face_cascade.load(face_cascade_name))
    {
        cout << "--(!)Error loading face cascade\n";
        //return -1;
    };
    //Load the eye cascades
    String eyes_cascade_name = "xml/haarcascade_eye_tree_eyeglasses.xml";
    if(!eyes_cascade.load(eyes_cascade_name))
    {
        cout << "--(!)Error loading eyes cascade\n";
        //return -1;
    };
}


void EyeGazing::detectAndDisplay(int ix, cv::Mat frame)
{
    cv::Mat frame_gray;
    cv::imwrite("frames/f" + std::to_string(ix) + ".png", frame);

    cv::cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
    cv::equalizeHist( frame_gray, frame_gray );

    //-- Detect faces
    std::vector<Rect> faces;

    //face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE|CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(150, 150) );
    face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(150, 150), cv::Size(350, 350));
    //face_cascade.detectMultiScale( frame_gray, faces );


    //for ( size_t i = 0; i < faces.size(); i++ )
    double leftX = 0;
    double leftY = 0;
    double rightX = 0;
    double rightY = 0;
    if(faces.size() == 1)
    {
        cv::Point center( faces[0].x + faces[0].width/2, faces[0].y + faces[0].height/2 );
        cv::ellipse( frame, center, Size( faces[0].width/2, faces[0].height/2 ), 0, 0, 360, Scalar( 255, 0, 255 ), 4 );

        Mat faceROI = frame_gray( faces[0] );
        
        //Mat gbImage;
        //cv::GaussianBlur(faceROI, gbImage, cv::Size(0, 0), 3);
        //cv::addWeighted(faceROI, 1.5, gbImage, -0.5, 0, gbImage);

        // sharpen image using "unsharp mask" algorithm
        Mat blurred; double sigma = 1, threshold = 5, amount = 1;
        GaussianBlur(faceROI, blurred, Size(), sigma, sigma);
        Mat lowContrastMask = abs(faceROI - blurred) < threshold;
        Mat sharpened = faceROI*(1+amount) + blurred*(-amount);
        faceROI.copyTo(sharpened, lowContrastMask);

        //-- In each face, detect eyes
        std::vector<Rect> eyes;
        
        //eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
        //eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0|CV_HAAR_DO_ROUGH_SEARCH, Size(30, 30) );
        //eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0|CV_HAAR_DO_CANNY_PRUNING, Size(30, 30) );
        //eyes_cascade.detectMultiScale( faceROI, eyes );

        eyes_cascade.detectMultiScale( sharpened, eyes, 1.1, 3, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );
        //eyes_cascade.detectMultiScale( sharpened, eyes );
        
        
        
        //std::cout << eyes.size() << std::endl;
        //std::cout << "(";

        //between 1 or 2
        int eyesNumber = eyes.size();
        if(eyesNumber > 0 && eyesNumber < 3){

            for ( size_t j = 0; j < eyesNumber; j++ )
            {
                Point eye_center(   faces[0].x + eyes[j].x + eyes[j].width/2, 
                                    faces[0].y + eyes[j].y + eyes[j].height/2 );
                int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );

                circle( frame, eye_center, radius, Scalar( 255, 0, 0 ), 4 );
                circle( frame, eye_center, 1, Scalar( 255, 255, 0 ), 4 );    

                if(j == 0){
                    leftX = eye_center.x;
                    leftY = eye_center.y;
                }
                else{
                    rightX = eye_center.x;
                    rightY = eye_center.y;
                }
            }    

            if(eyesNumber == 1){
                Rect roi_right = Rect(eyes[0].x, eyes[0].y, eyes[0].width, eyes[0].height);
                Mat image_right = faceROI(roi_right);
                //imshow( "EyeDetection right", image_right );    
            }

            if(eyesNumber == 2){
                Rect roi_left = Rect(eyes[1].x, eyes[1].y, eyes[1].width, eyes[1].height);
                Mat image_left = faceROI(roi_left);
                //imshow( "EyeDetection left", image_left );    
            }
            
        }
        

        

      
        //std::cout << eye_center.x << "," << eye_center.y << ",";
        
        
        if(leftX > 0 && leftY > 0 && rightX > 0 && rightY > 0){
            std::cout << ix << ": (" << leftX << "," << leftY << ") - (" << rightX << "," << rightY << ")" << std::endl;
            
            
            
        }
        

        


        //std::cout << ")" << std::endl;
        
        //-- Show what you got
        
        //imshow( "FaceDetection", sharpened );
        
        //imshow( "FaceDetection", faceROI );
    }
    LogFile << std::to_string(ix) << ",";
    LogFile << std::to_string((int)leftX) << ","; 
    LogFile << std::to_string((int)leftY) << ","; 
    LogFile << std::to_string((int)rightX) << ","; 
    LogFile << std::to_string((int)rightY) << "\n";

    leftX = 0;
    leftY = 0;
    rightX = 0;
    rightY = 0;
    imshow( "Original", frame );
    
}

void EyeGazing::closeFiles()
{
    LogFile.close();
}
