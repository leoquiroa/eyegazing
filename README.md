### RUN

Compiler and Linker
```
g++ -std=c++11 *.cpp `pkg-config --libs --cflags opencv` -o eyegazing
```
Execution
```
./eyegazing --camera 0 --eyes_cascade haarcascade_eye_tree_eyeglasses.xml --face_cascade haarcascade_frontalface_alt.xml 
```

### METHODS

```
cv::cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
```
Converts an image from one color space to another.
* src – input image: 8-bit unsigned, 16-bit unsigned ( CV_16UC... ), or single-precision floating-point.
* dst – output image of the same size and depth as src.
* RGB to Gray: Y = 0.299*R + 0.587*G + 0.114*B

```
cv::equalizeHist( frame_gray, frame_gray );
```
Equalizes the histogram of a grayscale image.
* src – Source 8-bit single channel image.
* dst – Destination image of the same size and type as src .

```
face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(150, 150), cv::Size(350, 350));
```
Detects objects of different sizes in the input image. The detected objects are returned as a list of rectangles.
* image – Matrix of the type CV_8U containing an image where objects are detected.
* objects – Vector of rectangles where each rectangle contains the detected object.
* scaleFactor – Parameter specifying how much the image size is reduced at each image scale.
* minNeighbors – Parameter specifying how many neighbors each candidate rectangle should have to retain it.
* flags – Parameter with the same meaning for an old cascade as in the function cvHaarDetectObjects. It is not used for a new cascade.
* minSize – Minimum possible object size. Objects smaller than that are ignored.
* maxSize – Maximum possible object size. Objects larger than that are ignored.

```
cout << "Width : " << frame.cols << endl;
cout << "Height: " << frame.rows << endl;
```
* Height: 480
* Width : 640

```
radiusL = sqrt(pow((leftX - 320),2) + pow((leftY - 240),2));
angleL = atan ((leftY - 240)/(leftX - 320)) * 180 / PI;
radiusR = sqrt(pow((rightX - 320),2) + pow((rightY - 240),2));
angleR = atan ((rightY - 240)/(rightX - 320)) * 180 / PI;
std::cout << "(" << radiusL << "," << angleL << ") - (" << radiusR << "," << angleR << ")" << std::endl;
```
* Distance from (x,y) to center
* angle from the two dots
