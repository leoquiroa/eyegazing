#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
//#include "opencv2/opencv.hpp"
#include <cmath>

#include <iostream>
#include <fstream>

using namespace std;
using namespace cv;

class EyeGazing {
    public:
        ofstream LogFile;
        cv::CascadeClassifier face_cascade;
        cv::CascadeClassifier eyes_cascade;

        EyeGazing();

        void openFiles();
        void detectAndDisplay(int ix, cv::Mat frame);
        void closeFiles();
    private:
        
};